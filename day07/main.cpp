#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#define BROJ1 100
#define BROJ2 1000
#define BROJ3 10000

//Provjeravamo je li sve u redu nakon zapisivanja file-a u vektor.
void prikaziVektor(std::vector<int> &vektor) {
    for (auto it = vektor.begin(); it != vektor.end(); it++) {
        std::cout << *it << std::endl;
    }

    std::cout << std::endl;
}


//Funkcija za citanje file-a.
void citajFile(std::fstream &ulaz, std::vector<int> &brojevi) {
    int broj = 0;

    char podaci = {};

    while (ulaz >> broj >> podaci) {
        brojevi.push_back(broj);
    }

    ulaz >> broj;
    brojevi.push_back(broj);
}

//Funkcija koja trazi najjaci signal koji moze biti poslan potisnicima.
void ampSoftware(std::fstream &izlaz, std::vector<int> brojevi, int inputA, int inputB, int &output) {
    int trenutna_poz = 0;
    int radPozicije1 = 0;
    int radPozicije2 = 0;
    int radPozicije3 = 0;

    //Kod 99 znaci prestanak rada.
    while (brojevi[trenutna_poz] != 99) {
        radPozicije1 = (brojevi[trenutna_poz] / BROJ1 % 10 == 1) ? trenutna_poz + 1 : (trenutna_poz + 1 < brojevi.size()) ? brojevi[trenutna_poz + 1] : radPozicije1;
        radPozicije2 = (brojevi[trenutna_poz] / BROJ2 % 10 == 1) ? trenutna_poz + 2 : (trenutna_poz + 2 < brojevi.size()) ? brojevi[trenutna_poz + 2] : radPozicije2;
        radPozicije3 = (brojevi[trenutna_poz] / BROJ3 % 10 == 1) ? trenutna_poz + 3 : (trenutna_poz + 3 < brojevi.size()) ? brojevi[trenutna_poz + 3] : radPozicije3;

        switch (brojevi[trenutna_poz] % 100) {
            case 1:
                  brojevi[radPozicije3] = brojevi[radPozicije2] + brojevi[radPozicije1];
                  trenutna_poz += 4;
                  break;

            case 2:
                  brojevi[radPozicije3] = brojevi[radPozicije2] * brojevi[radPozicije1];
                  trenutna_poz += 4;
                  break;

            case 3:
                  brojevi[radPozicije1] = inputA;
                  inputA = inputB;            
                  trenutna_poz += 2;
                  break;

            case 4:
                  output = brojevi[radPozicije1];
                  trenutna_poz += 2;
                  break;

            case 5:
                  trenutna_poz = brojevi[radPozicije1] != 0 ? brojevi[radPozicije2] : trenutna_poz + 3;
                  break;          

            case 6:
                  trenutna_poz = brojevi[radPozicije1] == 0 ? brojevi[radPozicije2] : trenutna_poz + 3;
                  break;

            case 7:
                  brojevi[radPozicije3] = brojevi[radPozicije1] < brojevi[radPozicije2];
                  trenutna_poz += 4;
                  break;

            case 8:
                  brojevi[radPozicije3] = brojevi[radPozicije1] == brojevi[radPozicije2];
                  trenutna_poz += 4;
                  break;                    
        }
    }
}

int main() {
    
    //Postavi kao input file.
    std::fstream ulaz ("input.txt", std::fstream::in);

    //Postavi kao output file.
    std::fstream izlaz ("output.txt", std::fstream::out);

    //Vektor u kojeg ćemo trpati brojeve iz datoteke.
    std::vector<int> brojevi;

    citajFile(ulaz, brojevi);

    prikaziVektor(brojevi);

    //Postavke faze pojacala.
    std::vector<int> fazePojacala = {0, 1, 2, 3, 4};

    int izlazPojacala = 0;
    int najveciSignal = 0;

    do {
        izlazPojacala = 0;

        for (auto it = fazePojacala.begin(); it != fazePojacala.end(); it++) {
            ampSoftware(izlaz, brojevi, *it, izlazPojacala, izlazPojacala);
        }

        najveciSignal = std::max(najveciSignal, izlazPojacala);
    }

    while (std::next_permutation(fazePojacala.begin(), fazePojacala.end()));

    izlaz << najveciSignal;

    ulaz.close();
    izlaz.close();
}