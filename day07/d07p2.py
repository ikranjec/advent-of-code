#Ubacujemo modul koji vraca permutacije (sve moguce kombinacije) duljine r
# permutations('ABCD', 2) --> AB AC AD BA BC BD CA CB CD DA DB DC
# permutations(range(3)) --> 012 021 102 120 201 210

from itertools import permutations 

#Otvori datoteku input.txt
datoteka = open("input.txt", "r")

#Vrati listu integera, s tim da pobrises zareze
datoteka = list(map(int, datoteka.readline().split(',')))

#Testiranje ispisa onoga sto se nalazi u "datoteka"
#for linija in datoteka:
#    print(linija)

maxSignal = 0

#Trebamo razlicite kombinacije faznih postavki za pojacala u loop mode-u
for kombinacije in permutations(range(5, 10)):
    input = [[faznePostavke] for faznePostavke in kombinacije]
    progs = [list(datoteka) for faznePostavke in kombinacije]
    input[0] += [0]
    pokrenuto = [True] * 5
    pcs = [0] * 5

    #Cekamo da se cijeli proces zaustavi kod neke kombinacije u listi progs
    while (True in pokrenuto):
        for trenutna_instrukcija in range(5):
            #Restore-aj memoriju
            prog = progs[trenutna_instrukcija]
            #Restore-aj progamski counter
            pc = pcs[trenutna_instrukcija]
            #Restore-aj input
            inp = input[trenutna_instrukcija]

            while (True):
               (x, cmd), args = divmod(prog[pc], 100), []
               for (i, rc) in enumerate([[],[1, 1, 0], [1, 1, 0], [0], [1], [1, 1], [1, 1], [1, 1, 0], [1, 1, 0]][cmd % 99]):
                  args += [prog[pc+i+1]]
                  if x % 10 == 0 and rc != 0: args[-1] = prog[args[-1]]
                  x //= 10
               orig_pc = pc
               if (cmd == 1): 
                  #Zbrajanje
                  prog[args[2]] = args[0] + args[1]

               elif (cmd == 2):
                  #Mnozenje
                  prog[args[2]] = args[0] * args[1] # multiply

               elif (cmd == 3):
                  if (len(inp) == 0):
                     #Ulaz
                     break
                  prog[args[0]] = inp.pop(0)

               elif (cmd == 4):
                  #Izlaz
                  #Dodaj to ulazu sljedeceg pojacala
                  input[(trenutna_instrukcija + 1) % 5] += [args[0]]
                  if (trenutna_instrukcija == 4):
                     res = args[0]

               elif (cmd == 5 and args[0] != 0):
                  #Branch if true
                  pc = args[1] 

               elif (cmd == 6 and args[0] == 0):
                  #Branch if false
                  pc = args[1] 

               elif (cmd == 7):
                  #Provjeri je li manje
                  prog[args[2]] = int(args[0] < args[1]) 

               elif (cmd == 8):
                  #Provjeri je li jednako
                  prog[args[2]] = int(args[0] == args[1]) 

               elif (cmd == 99):
                       #STOP
                       pokrenuto[trenutna_instrukcija] = False
                       break

               if (pc == orig_pc):
                       #Odi na sljedecu instrukciju samo ako nije bilo 'skoka'
                       pc += len(args) + 1 

                 #Spremi program counter      
            pcs[trenutna_instrukcija] = pc  
   
    if (res > maxSignal):
     maxSignal = res 
     maxp = kombinacije
print(maxSignal, maxp)              



