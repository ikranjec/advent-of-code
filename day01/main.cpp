#include<iostream>
#include<fstream>
#include<cmath>
#include<vector>

//Izracun fuel-a
void fuelCalc(std::vector<double> &polje, int &fuel) {
    for (auto it = polje.begin(); it !=polje.end(); it++) {
        fuel += floor(*it / 3) - 2;
    }
}

//Izracun fuel-a za fuel
double totalFuelCalc(std::vector<double> &polje) {
    double tmp = 0;
    for (int i = 0; i < polje.size(); i++) {
        double varijabla = floor(polje[i] / 3) - 2;

        while (varijabla > 0) {
            tmp += varijabla;
            varijabla = floor(varijabla / 3) - 2;
        }
    }
    return tmp;
}

int main() {
    //Lokacija datoteke
    const char*  filename = "/home/ikranjec/Radna površina/projekti_test/advent_of_code/input.txt";

    //Ukupna kolicina fuel-a
    int fuel = 0;

    //Vektor u kojeg ćemo trpati int vrijednosti
    std::vector<double> polje;

    //Otvaramo datoteku
    std::ifstream mojaDatoteka(filename);
    if (mojaDatoteka) {

        //Hvatamo svaku liniju u varijablu vrijednost
        int vrijednost;

        //Sve dok postoji mogucnost zapisivanja iz mojaDatoteka
        while (mojaDatoteka >> vrijednost) {

            //U vektor ubacuj varijablu vrijednost
            polje.push_back(vrijednost);
        }
    }

    fuelCalc(polje, fuel);

    std::cout << "Gotovo!! \n" ; 
    std::cout << "\t Fuel: " << fuel << std::endl;
    std::cout << "\t Total fuel: " << totalFuelCalc(polje) << std::endl;

    mojaDatoteka.close();
}