#Module koji pruza podrsku za rad sa regex-ox 
import re

#Otvaramo datoteku
datoteka = open("input.txt", "r")

#Definiramo klasu planet
class Planet():

    #Konstruktor
    def __init__(self, name, parent):
        self.name = name
        self.parent = parent
        self.children = []

    def ukupno_orbita(self):
        if (self.parent == None):
            return 0

        return 1 + self.parent.ukupno_orbita() 

    def getIme(self):
        return self.name

    def getOrbit(self):
        children = [child.name for child in self.children]
        return str(children) + "->" + self.name + "->" + str(self.parent)

planets = []

for linija in datoteka:

    #Razdvajamo tekst iz datoteke u body_name i planet_name
    #Razdvajamo tekst na ")" znaku
    body_name, planet_name = linija.rstrip().split(")")
    
    satelit = None
    body = None

    for p in planets:
        if (planet_name == p.name):
            satelit = p

        if (body_name == p.name):
            body = p    


    if (satelit != None and body != None):
        satelit.parent = body
        body.children.append(satelit)

    elif (satelit != None and body == None):
        body = Planet(body_name, None)
        body.children.append(satelit)
        satelit.parent = body
        planets.append(body)

    elif (satelit == None and body != None):
        satelite = Planet(planet_name, body)
        body.children.append(satelite)
        planets.append(satelite)    

    elif (satelit == None and body == None):
        body = Planet(body_name, None)
        satelite = Planet(planet_name, body)
        body.children.append(satelite)
        planets.append(body)
        planets.append(satelite)


def prviDio():
    rezultat = 0
    
    for planet_name in planets:
        rezultat += planet_name.ukupno_orbita()

    return rezultat    

def drugiDio():
    you = None

    for p in planets:
        if (p.name == "YOU"):
            you = p

    rezultat = 0
    trazeno = [you]
    cekanje = [you]
    put = []

    while (len(cekanje) > 0):
        item = cekanje[-1]

        if (item == None):
            cekanje.pop()
            continue

        while (len(put) != 0):
            if (item not in put[-1].children and item != put[-1].parent):
                put.pop()
            else:
                break

        if (len(item.children) != 0):
            put.append(item)

        names = [c.name for c in item.children]

        if ("SAN" in names):
            break

        cekanje.pop()

        if (item.parent not in trazeno):
            cekanje.append(item.parent)
            trazeno.append(item.parent)

        for child in item.children:
            if (child not in trazeno):
                cekanje.append(child)
                trazeno.append(child)

    return len(put) - 1

p1 = prviDio()
p2 = drugiDio()
print("Rezultat part 1: " ,p1)
print("Rezultat part 2: ", p2)








