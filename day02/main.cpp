#include<iostream>
#include<fstream>
#include<vector>

//Provjeravamo je li sve u redu nakon zapisivanja file-a u vektor.
void prikaziVektor(std::vector<int> &vektor) {
    for (auto it = vektor.begin(); it != vektor.end(); it++) {
        std::cout << *it << " " ;
    }

    std::cout << std::endl;
}

//Funkcija za citanje file-a.
void citajFile(std::fstream &ulaz, std::vector<int> &polje) {
    int vrijednost = 0;

    char podaci = {};

    while (ulaz >> vrijednost >> podaci) {
        polje.push_back(vrijednost);
    }

    ulaz >> vrijednost;
    polje.push_back(vrijednost);
}

int brojElemenata(std::vector<int> &polje) {
    int count = 0;

    for (auto it = polje.begin(); it != polje.end(); it++) {
        count++;
    }
    
    return (count - 1);
}

//Problem je nastao kod racunanja broja elemenata
//Zbog toga je pri pokretanju programa izbacivao segmentation error, core depleted
void intcodeFix(std::vector<int> &polje, std::fstream &izlaz) {
    for (int i = 0; i < brojElemenata(polje); i += 4) {
        switch(polje[i])
        {
            //opcode 1
            //gledamo zbroj dvaju elemenata
            //gledamo dva broja koja se nalaze poslije pozicije opcode-a
            //zbrajamo brojeve na tim pozicijama i zbroj zapisujemo na poziciju polje[i + 3]
            case 1:
                polje[polje[i + 3]] = polje[polje[i + 1]] + polje[polje[i + 2]];
                break;

            //opcode 2
            //gledamo umnozak dvaju elemenata
            //gledamo dva broja koja se nalaze poslije pozicije opcode-a
            //mnozimo brojeve na tim pozicijama i umnozak zapisujemo na poziciju polje[i + 3]    
            case 2:
                polje[polje[i + 3]] = polje[polje[i + 1]] * polje[polje[i + 2]];
                break;    
        }
    }
    izlaz << polje[0];
}



int main() {

    //Postavi kao input file.
    std::fstream ulaz ("input.txt" , std::fstream::in);

    //Postavi kao output file.
    std::fstream izlaz ("izlaz.txt", std::fstream::out);

    //Vektor u kojeg ćemo trpati vrijednosti cijele prve linije input file-a.
    std::vector<int> polje;

    citajFile(ulaz, polje);
    
    //Postavljamo vrijednost vektora kako je napisano u uputama.
    polje[1] = 12;
    polje[2] = 2;

    std::cout << "------------PRVI DIO ZADATKA------------" << std::endl;
    std::cout << "Broj elemenata: " << brojElemenata(polje) << std::endl;
    prikaziVektor(polje);
    std::cout << std::endl;
    intcodeFix(polje, izlaz);
    prikaziVektor(polje);
    std::cout << std::endl;
    ulaz.close();
    izlaz.close();
}