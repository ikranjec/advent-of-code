#include<iostream>
#include<fstream>
#include<vector>

//Funkcija za citanje file-a.
void citajFile(std::fstream &ulaz, std::vector<int> &polje) {
    int vrijednost = 0;

    char podaci = {};

    while (ulaz >> vrijednost >> podaci) {
        polje.push_back(vrijednost);
    }

    ulaz >> vrijednost;
    polje.push_back(vrijednost);
}

int brojElemenata(std::vector<int> &polje) {
    int count = 0;

    for (auto it = polje.begin(); it != polje.end(); it++) {
        count++;
    }
    
    return (count - 1);
}

int main() {

    //Postavi kao input file.
    std::fstream ulaz ("input.txt" , std::fstream::in);

    //Postavi kao output file.
    std::fstream izlaz ("izlaz2.txt", std::fstream::out);

    //Vektor u kojeg ćemo trpati vrijednosti cijele prve linije input file-a.
    std::vector<int> polje;

    citajFile(ulaz, polje);

    std::vector<int> aux(polje);

    //Trazimo outpu noun * 100 + verb koji daje 19690720
 	for (int noun = 0; noun < 100; noun++) {
		for (int verb = 0; verb < 100; verb++) {
			polje = aux;
			polje[1] = noun;
			polje[2] = verb;

			for (int pozicija = 0; polje[pozicija] != 99; pozicija += 4) {
				switch ((polje[pozicija]))
				{
				case 1:
					polje[polje[pozicija + 3]] = polje[polje[pozicija + 1]] + polje[polje[pozicija + 2]];
					break;
				case 2:
					polje[polje[pozicija + 3]] = polje[polje[pozicija + 1]] * polje[polje[pozicija + 2]];
					break;
				}
			}

            //Kada dođemo do kombinacije da prvi element bude jednak zeljenom outputu, ispisi u izlaznu datoteku noun * 100 + verb
			if (polje[0] == 19690720) {
				izlaz << noun * 100 + verb;

                //postavi noun i verb na takve vrijednosti da izletis iz petlje u ovom koraku
				noun = 100;
				verb = 100;
			}
		}
	}
}
