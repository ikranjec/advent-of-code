#Python imaging library
#Koristimo ga kako bi na kraju dobili sliku
from PIL import Image

#Otvaramo datoteku
datoteka = open("input.txt", "r")
kod = datoteka.read()
datoteka.close()

dimenzije = 25*6
duljinaKod = len(kod)
brSlojeva = int(duljinaKod / dimenzije)
pikselaPoSloju = int(duljinaKod / brSlojeva)

pixeli = kod
polje = []
while (len(pixeli) > 0):
    polje.append(pixeli[0:150])
    pixeli = pixeli[150:]
brNula = (list(sloj.count("0") for sloj in polje))

#Dimenzije slike
x = 25
y = 6

slika = Image.new("RGB", (x, y), color = "Red")

map = slika.load()

for layer in polje:
    brojac = 0
    for a in range(0, y):
        for b in range(0, x):
            if map[b, a] == (255,0,0):
                if layer[brojac] == "0":
                    map[b, a] = (0,0,0)
                elif layer[brojac] == "1":
                    map[b, a] = (255, 255, 255)
            brojac += 1         

slika.show()