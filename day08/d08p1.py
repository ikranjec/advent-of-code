#Otvaramo datoteku
datoteka = open("input.txt", "r")
kod = datoteka.read()
datoteka.close()

dimenzije = 25 * 6
minBrojNula = dimenzije + 1
rezultat = 0

for i in range(len(kod) // dimenzije):
    sloj = kod[i * dimenzije : (i + 1) * dimenzije]
    brojNula = sloj.count("0")
    if (brojNula < minBrojNula):
        minBrojNula = brojNula
        rezultat = sloj.count("1") * sloj.count("2")

print("Prvi dio: %d " %rezultat)