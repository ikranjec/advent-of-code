#include <algorithm>
#include <array>
#include <iostream>

//Validacija lozinke
int provjeraLozinke(const std::array<int, 6> &polje) {

    //U slucaju da su nam brojevi u arrayu sortirani
    //Primjer: 123456 ---> tad vrati nulu, nije validna lozinka
    if(!std::is_sorted(polje.begin(), polje.end())) {
        return 0;
    }

    //Ugrađenom funkcijom adjacent_find trazimo parove
    if(std::adjacent_find(polje.begin(), polje.end()) != polje.end()) {
        std::array<int, 10> znamenke = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        for(int i = 0; i < 6; i++) {
            znamenke[polje[i]]++;
        }

        //Ako sadrži dva ista broja vrati 2
        if(std::find(znamenke.begin(), znamenke.end(), 2) != znamenke.end()) {
            return 2;
        }

        //Inače vrati 1
        return 1;
    }

    //U slučaju da lozinka nije validna
    return 0;
}

//Funkcija za hodanje po polju
void povecajPolje(std::array<int, 6> &polje, int pozicija) {

    //Ako je pozicija broja u polju veca od velicine u polju
    //Ili manja od 0, vrati se iz rekurzivne funkcije
    if (pozicija >= polje.size() || pozicija < 0) {
        return;
    }

    //Pomakni se po polju
    polje[pozicija]++;

    //Pozovi rekurzivnu funkciju i pomici se po polju
    if (polje[pozicija] == 10 && pozicija != 0) {
        polje[pozicija] = 0;
        povecajPolje(polje, pozicija - 1);
    }
}

int main() {
    int max = 678275;
    int min = 130254;

    int ukupnoPart1 = 0;
    int ukupnoPart2 = 0;

    //Minimalni broj u range-u u kojem se moze nalaziti password
    std::array<int, 6> lozinka = {1, 3, 0, 2, 5, 4};

    for(int i = 0; i < (max - min); i++) {
        int valjanost = provjeraLozinke(lozinka);
        if(valjanost == 2) {
            ukupnoPart1++;
            ukupnoPart2++;
        } else if(valjanost == 1) {
            ukupnoPart1++;
        }
        povecajPolje(lozinka, lozinka.size() - 1);
    }
    std::cout << "Prvi dio: " << ukupnoPart1 << std::endl;
    std::cout << "Drugi dio: " << ukupnoPart2 << std::endl;
    return 0;
}