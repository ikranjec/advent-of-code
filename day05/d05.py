#Učitaj string iz file-a i prekopiraj ga u polje
datoteka = open("input.txt" , "r")
kod = datoteka.read()
datoteka.close()
kod = kod.split(",")

#Pretvori elemente polja iz stringa u int
for i in range(len(kod)):
    kod[i] = int(kod[i])

#Funkcija koja vraca polje od pet elemenata koje predstavlja intcode
def kodToPolje(var):
    intcode = list(str(var))

    if (len(intcode) < 5 ):
        for i in range (5 - len(intcode)):
            intcode.insert(0, "0")

    for i in range (len(intcode)):
        intcode[i] = int(intcode[i])
    return intcode

# instrukcija[4] && instrukcija[3] ----> opcode
# instrukcija[2] način rada prvog parametra
# instrukcija[1] način rada drugog parametra 

#Funkcija koja vraća vrijednost pozicije s obzirom na njen način rada
def value(nacin_rada, pozicija):
    vrijednost = 0

    if (nacin_rada == 0):
        vrijednost = kod[kod[pozicija]]

    else:
        vrijednost = kod[pozicija]

    return vrijednost 


pos = 0

#Pokreni intcode
while(kod[pos] != 99):

    instrukcija = kodToPolje(kod[pos])

    #Zbrajanje
    if (instrukcija[4] == 1):

        dva = pos + 1
        tri = pos + 2
        cetiri = kod[pos + 3]

        kod[cetiri] = value(instrukcija[2], dva) + value(instrukcija[1], tri)
        pos += 4

    #Mnozenje
    elif (instrukcija[4] == 2):

        dva = pos + 1
        tri = pos + 2
        cetiri = kod[pos + 3]

        kod[cetiri] = value(instrukcija[2], dva) * value(instrukcija[1], tri)
        pos += 4

    #Uzmi input od usera
    elif(instrukcija[4] == 3):
        print("Unesite ulaznu vrijednost: ")
        odabir = input()
        odabir = int(odabir)    
        
        if (instrukcija[2] == 0):
            kod[kod[pos + 1]] = odabir

        else:
            kod[pos + 1] = odabir

        pos += 2                

    #Ispisi izlaznu vrijednost
    elif(instrukcija[4] == 4):
        print(value(instrukcija[2], (pos + 1)))
        pos += 2    

