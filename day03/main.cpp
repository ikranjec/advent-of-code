#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <algorithm>
#include <map>

//Funkcija koja  ce uzeti file i 'pratiti' smjer zice

std::vector<std::pair<bool, int>> trace(std::string input) {

    //U buffer cemo spremati sve osim zareza
	std::string buffer;

    //U vektor cemo spremati jednu strukturu pair koja u sebi moze pohraniti dvije primitivne vrijednosti
	std::vector<std::pair<bool, int>> directions;

    //Trpamo sve osim zareza u string iz datoteke
    //Ukoliko petlja naleti na ',' onda gleda prvi clan stringa koji ce nam reci smjer
    /*
    Kada naides na 'R' promijeni to u + i smjer u pairu postavi na true
    Kada naides na 'L' promijeni to u - i smjer u pairu postavi na true
    Kada naides na 'U' promijeni to u + i smjer u pairu postavi na false
    Kada naides na 'D' promijeni to u - i smjer u pairu postavi na false
    */
	for (int i = 0; i <= input.size(); ++i) {
		if (input[i] != ',') {
			buffer.push_back(input[i]);
		}
		else {
			std::pair<bool, int> direction;
			switch (buffer[0]) {
				case 'R':
					buffer[0] = '+';
					direction.first = true;
					break;
				case 'L':
					buffer[0] = '-';
					direction.first = true;
					break;
				case 'U':
					buffer[0] = '+';
					direction.first = false;
					break;
				case 'D':
					buffer[0] = '-';
					direction.first = false;
					break;
			}

            //Ukupnu vrijednost koja se nalazi u strukturi pair pod varijablom int pretvori u broj, a zatim tu strukturu uguraj u strukturu directions
			direction.second = std::stoi(buffer);
			directions.push_back(direction);

            //Zbrisi string buffer
			buffer.clear();
		}
	}
    //Funkcija vraca upute u obliku strukture pair koja u sebi ima bool i int
	return directions;
}

int main() {
    //Datoteku razdvojimo na dvije linije pomocu ovog stringa
	std::string lines[2];

    //Prva linija input.txt se ispisuje u lines[0] a druga u lines[1]
	std::ifstream("input.txt") >> lines[0] >> lines[1];

    //U ovaj vektor vracamo vrijednost koju dobijemo nakon sto izvrsimo funkciju trace na liniji 1 i liniji 2
	std::vector<std::pair<bool, int>> directions[] = { trace(lines[0]), trace(lines[1]) };

	std::set<int> distances, steps;

	std::set<std::pair<int, int>> intersections;

	std::map<std::pair<int, int>, int[2]> grid;

	for (int pos = 0; pos <= 1; ++pos) {
		int tX = 0, tY = 0, tS = 0;
		for (std::pair<bool, int> dir : directions[pos]) {
			int dX = dir.first ? dir.second : 0;
			int dY = dir.first ? 0 : dir.second;

			while (dX != 0 || dY != 0) {
				if (dX != 0) {
					tX += dX > 0 ? +1 : dX < 0 ? -1 : 0;
					dX += dX > 0 ? -1 : dX < 0 ? +1 : 0;
					++tS;
					grid[{tX, tY}][pos] = grid[{tX, tY}][pos] == 0 ? tS : grid[{tX, tY}][pos];
				}
				if (dY != 0) {
					tY += dY > 0 ? +1 : dY < 0 ? -1 : 0;
					dY += dY > 0 ? -1 : dY < 0 ? +1 : 0;
					++tS;
					grid[{tX, tY}][pos] = grid[{tX, tY}][pos] == 0 ? tS : grid[{tX, tY}][pos];
				}
				if (grid[{tX, tY}][0] != 0 && pos == 1) {
					intersections.insert({tX, tY});
					distances.insert(abs(tX) + abs(tY));
					steps.insert(grid[{tX, tY}][0] + grid[{tX, tY}][1]);
				}
			}
		}
	}

	std::cout << "Part I: " << *std::min_element(distances.begin(), distances.end()) << std::endl;

	std::cout << "Part II: " << *std::min_element(steps.begin(), steps.end()) << std::endl;
}